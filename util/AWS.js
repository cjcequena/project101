
import { CognitoUserPool, CognitoUserAttribute } from 'amazon-cognito-identity-js';
import Messages from './Messages';
// Initialize cognito credentials
const cognitoUserPool = () => {
  const poolData = {
    UserPoolId: process.env.COGNITO_USER_POOL_ID,
    ClientId: process.env.COGNITO_CLIENT_ID,
  };
  return new CognitoUserPool(poolData);
};

const cognitoAttribute = (key, value) => {
  if (!key || !value) throw new Error(Messages.ERROR_INVALID_COGNITO_ATTRIBUTES);
  return new CognitoUserAttribute({
    Name: key,
    Value: value,
  });
};

export default { cognitoUserPool, cognitoAttribute };
