export default {
  ERROR_INVALID_CREDENTIALS: 'Inavlid Credentials',
  ERROR_INVALID_COGNITO_ATTRIBUTES: 'Invalid Cognito Attributes',
  ERROR_INVALID_REQUEST: 'Invalid Request',
  ERROR_NOT_VALID_TOKEN: 'Not a Valid Token',
  ERROR_RECORD_NOT_FOUND: 'No Records found',
};
