import { Messages } from '../util';
import { TripRating, Trip } from '../models';

const express = require('express');

const router = express.Router();

/* GET Trips listing */
router.get('/:tripId', (req, res) => {
  const { tripId } = req.params;
  Trip.find({ trip_id: tripId }, {}).then((result) => {
    res.status(200).json(result);
  }).catch(error => res.status(400).json({ error }));
});

/**
 * Retrieve All ratings of single trip
 */
router.get('/:id/ratings', async (req, res) => {
  const tripId = req.params.id;

  const tripRatings = await TripRating.findOne({ tripId });

  if (!tripRatings) {
    return res.status(400).json({ error: Messages.ERROR_RECORD_NOT_FOUND });
  }
  return res.status(200).json({ tripRatings });
});

/**
 * Retrieve single rating
 */
router.get('/:id/ratings/:ratingId', async (req, res) => {
  const tripId = req.params.id;
  const { ratingId } = req.params;

  TripRating.findOne({ tripId })
    .then((data) => {
      const ratingItem = data.ratings.filter(x => x.id === ratingId);
      if (ratingItem.length === 0) {
        return res.status(400).json({ error: Messages.ERROR_RECORD_NOT_FOUND });
      }
      return res.status(200).json(ratingItem[0]);
    });
});

/**
 * Add new Trip Ratings
 */
router.put('/:id/ratings', async (req, res) => {
  const {
    passengerId,
    rating,
    comments,
  } = req.body;
  const tripId = req.params.id;

  // check if required data exists
  if (!tripId || !passengerId || !rating) {
    return res.status(400).json({ error: Messages.ERROR_INVALID_REQUEST });
  }

  const passengerTrip = await Trip.findOne({ trip_id: tripId, passenger_id: passengerId, trip_status: 'Completed' });
  // check if passenger and trip exists
  if (!passengerTrip) {
    return res.status(400).json({ error: Messages.ERROR_INVALID_REQUEST });
  }

  // if passenger id is invalid send an error message
  const ratingItem = await TripRating.findOne({ tripId });

  // Check if TriRating record exists
  if (ratingItem) {
    const ratingsData = {
      passengerId,
      rating,
      comments,
    };

    const passengerMatch = ratingItem.ratings
      .filter(x => x.passengerId === ratingsData.passengerId);

    // Checks if passenger has already rated and sends an error message
    if (passengerMatch.length > 0) {
      return res.status(400).json({ error: Messages.ERROR_INVALID_REQUEST });
    }

    ratingItem.ratings.push(ratingsData);
    return ratingItem.save()
      .then(result => res.status(200).json(result))
      .catch(error => res.status(400).json(error));
  }

  // If data does not exists create record
  const ratingsData = {
    tripId,
    ratings: [
      {
        passengerId,
        rating,
        comments,
      },
    ],
  };
  return TripRating.create(ratingsData)
    .then(result => res.status(200).json(result))
    .catch(error => res.status(400).json(error));
});


module.exports = router;
