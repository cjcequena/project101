import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
  firstName: String,
  divisinId: String,
  middleName: String,
  password: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    unique: true,
  },
  lastName: String,
  passengerCardId: String,
  mobilityAids: String,
  isPara: Boolean,
  address: {
    name: String,
    street_num: String,
    at_street: String,
    city: String,
    state: String,
    zip: String,
    location: {
      type: String,
      coordinates: [Number],
    },
  },
  deleted: {
    type: Boolean,
    default: false,
  },
}, { timestamps: true });

export default mongoose.model('Users', UserSchema);
