/* eslint-disable import/prefer-default-export */
import TripRating from './TripRating';
import Trip from './Trip';
import User from './User';

export {
  TripRating,
  Trip,
  User,
};
