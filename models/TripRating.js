import mongoose, { Schema } from 'mongoose';

const RatingSchema = new Schema({
  passengerId: {
    type: Number,
    required: true,
  },
  rating: {
    type: Number,
    required: true,
  },
  comment: String,
  deleted: {
    type: Boolean,
    default: false,
  },
}, { timestamps: true });

const TripRatingSchema = new Schema({
  tripId: {
    type: Number,
    required: true,
  },
  ratings: [RatingSchema],
  deleted: {
    type: Boolean,
    default: false,
  },
}, { timestamps: true });

export default mongoose.model('TripRatings', TripRatingSchema);
